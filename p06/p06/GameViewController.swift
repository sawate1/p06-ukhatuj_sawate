//
//  GameViewController.swift
//  p06
//
//  Created by Shivani Awate on 4/6/17.
//  Copyright © 2017 shivani. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {
    var activePlayer = 1 //this is the cross
    var gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0] //initialize the array
    
    let winningCombinations = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]] //all of the winning combinations
    
    var gameIsActive = true
    
    @IBOutlet weak var label: UILabel!
    
    var xwin = 0
    var draw = 0
    var owin = 0
   
let gameOver = UIView(frame: CGRect(x: 0, y: 130, width: 0, height: 0))
    
    
    @IBAction func action(_ sender:  AnyObject)
    {
        if (gameState[sender.tag-1] == 0 && gameIsActive == true)
        {
            gameState[sender.tag-1] = activePlayer //dosen't allow to over ride the game piece
            
            if (activePlayer == 1)
            {
                sender.setImage(UIImage(named:"Cross.png"), for: UIControlState())  //either will display a cross
                activePlayer = 2 // when the user has placed the brick we have to change to the opposite
            }
            else
            {
                sender.setImage(UIImage(named:"Nought.png"), for: UIControlState())  // or it will display a nought
                activePlayer = 1 // constantly change between the active players
            }
        }
        
        for combination in winningCombinations
        {
            if gameState[combination[0]] != 0 && gameState[combination[0]] == gameState[combination[1]] && gameState[combination[1]] == gameState[combination[2]]
            {
                gameIsActive = false
                
                if gameState[combination[0]] == 1
                {
                    //"CROSS HAS WON!"
                    
                    label.text = "CROSS HAS WON!"
                    xwin = UserDefaults.standard.integer(forKey: "xwin")
                    xwin = xwin + 1
                    UserDefaults.standard.set(xwin, forKey: "xwin")
                    gameOver.isHidden = false
                    
                    gameOver.backgroundColor = UIColor.init(colorLiteralRed: 0.0/255, green: 119.0/255, blue: 190.0/255, alpha: 1)
                    self.view.addSubview(gameOver)
                    
                    let codedLabel:UILabel = UILabel()
                    codedLabel.frame = CGRect(x: 100, y: 10, width: 200, height: 40)
                    codedLabel.textAlignment = .center
                    codedLabel.text = "Crosses Won"
                    codedLabel.numberOfLines=1
                    codedLabel.textColor=UIColor.black
                    codedLabel.font=UIFont.systemFont(ofSize: 22)
                    codedLabel.backgroundColor=UIColor.lightGray
                    gameOver.addSubview(codedLabel)
                    
                    //label
                    // draw count
                    let codedLabel2:UILabel = UILabel()
                    codedLabel2.frame = CGRect(x:100 , y: 85, width: 200, height: 40)
                    codedLabel2.textAlignment = .center
                    codedLabel2.text = "Total Cross Wins"
                    codedLabel2.numberOfLines=1
                    codedLabel2.textColor=UIColor.black
                    codedLabel2.font=UIFont.systemFont(ofSize: 22)
                    codedLabel2.backgroundColor=UIColor.lightGray
                    gameOver.addSubview(codedLabel2)
                    
                    
                    // draw count
                    let codedLabel1:UILabel = UILabel()
                    codedLabel1.frame = CGRect(x:100 , y: 125, width: 200, height: 40)
                    codedLabel1.textAlignment = .center
                    let s = UserDefaults.standard.integer(forKey: "xwin")
                    codedLabel1.text = String(s)
                    codedLabel1.numberOfLines=1
                    codedLabel1.textColor=UIColor.black
                    codedLabel1.font=UIFont.systemFont(ofSize: 22)
                    codedLabel1.backgroundColor=UIColor.lightGray
                    gameOver.addSubview(codedLabel1)
                    
                    //Call whenever you want to show it and change the size to whatever size you want
                    UIView.animate(withDuration: 0, animations: {
                        self.gameOver.frame.size = CGSize(width: 550, height: 375)
                    })

                }
                else
                {
                   // "NOUGHT HAS WON!"
                    
                    label.text = "NOUGHT HAS WON!"
                    owin = UserDefaults.standard.integer(forKey: "owin")
                    owin = owin + 1
                    UserDefaults.standard.set(owin, forKey: "owin")
                    gameOver.isHidden = false
                    
                    gameOver.backgroundColor = UIColor.init(colorLiteralRed: 0.0/255, green: 119.0/255, blue: 190.0/255, alpha: 1)
                    self.view.addSubview(gameOver)
                    
                    let codedLabel:UILabel = UILabel()
                    codedLabel.frame = CGRect(x: 100, y: 10, width: 200, height: 40)
                    codedLabel.textAlignment = .center
                    codedLabel.text = "Noughts Won"
                    codedLabel.numberOfLines=1
                    codedLabel.textColor=UIColor.black
                    codedLabel.font=UIFont.systemFont(ofSize: 22)
                    codedLabel.backgroundColor=UIColor.lightGray
                    gameOver.addSubview(codedLabel)
                    
                    //label
                    // draw count
                    let codedLabel2:UILabel = UILabel()
                    codedLabel2.frame = CGRect(x:100 , y: 85, width: 200, height: 40)
                    codedLabel2.textAlignment = .center
                    codedLabel2.text = "Total Nought Wins"
                    codedLabel2.numberOfLines=1
                    codedLabel2.textColor=UIColor.black
                    codedLabel2.font=UIFont.systemFont(ofSize: 22)
                    codedLabel2.backgroundColor=UIColor.lightGray
                    gameOver.addSubview(codedLabel2)
                    
                    
                    // draw count
                    let codedLabel1:UILabel = UILabel()
                    codedLabel1.frame = CGRect(x:100 , y: 125, width: 200, height: 40)
                    codedLabel1.textAlignment = .center
                    let s = UserDefaults.standard.integer(forKey: "owin")
                    codedLabel1.text = String(s)
                    codedLabel1.numberOfLines=1
                    codedLabel1.textColor=UIColor.black
                    codedLabel1.font=UIFont.systemFont(ofSize: 22)
                    codedLabel1.backgroundColor=UIColor.lightGray
                    gameOver.addSubview(codedLabel1)
                    
                    //Call whenever you want to show it and change the size to whatever size you want
                    UIView.animate(withDuration: 0, animations: {
                        self.gameOver.frame.size = CGSize(width: 550, height: 375)
                    })
                }
                
               playAgainButton.isHidden = false
               label.isHidden = false
            }
        }
        gameIsActive = false
        
        for i in gameState
        {
            if i == 0
            {
                gameIsActive = true
                break
            }
        }
        
        if gameIsActive == false
        {
            label.text = "DRAW"
            label.isHidden = false
            playAgainButton.isHidden = false
           // let gameOver = UIView(frame: CGRectMake(100, 100, 0, 0))
            draw = UserDefaults.standard.integer(forKey: "draw")
            draw = draw + 1
             UserDefaults.standard.set(draw, forKey: "draw")
            gameOver.isHidden = false

            gameOver.backgroundColor = UIColor.init(colorLiteralRed: 0.0/255, green: 119.0/255, blue: 190.0/255, alpha: 1)
            self.view.addSubview(gameOver)
            
            let codedLabel:UILabel = UILabel()
            codedLabel.frame = CGRect(x: 100, y: 10, width: 200, height: 40)
            codedLabel.textAlignment = .center
            codedLabel.text = "Match Draw"
            codedLabel.numberOfLines=1
            codedLabel.textColor=UIColor.black
            codedLabel.font=UIFont.systemFont(ofSize: 22)
            codedLabel.backgroundColor=UIColor.lightGray
            gameOver.addSubview(codedLabel)
            
            //label
            // draw count
            let codedLabel2:UILabel = UILabel()
            codedLabel2.frame = CGRect(x:100 , y: 85, width: 200, height: 40)
            codedLabel2.textAlignment = .center
            codedLabel2.text = "Total Draws"
            codedLabel2.numberOfLines=1
            codedLabel2.textColor=UIColor.black
            codedLabel2.font=UIFont.systemFont(ofSize: 22)
            codedLabel2.backgroundColor=UIColor.lightGray
            gameOver.addSubview(codedLabel2)
            
            
            // draw count
            let codedLabel1:UILabel = UILabel()
            codedLabel1.frame = CGRect(x:100 , y: 125, width: 200, height: 40)
            codedLabel1.textAlignment = .center
            let s = UserDefaults.standard.integer(forKey: "draw")
            codedLabel1.text = String(s)
            codedLabel1.numberOfLines=1
            codedLabel1.textColor=UIColor.black
            codedLabel1.font=UIFont.systemFont(ofSize: 22)
            codedLabel1.backgroundColor=UIColor.lightGray
            gameOver.addSubview(codedLabel1)
            
            //Call whenever you want to show it and change the size to whatever size you want
            UIView.animate(withDuration: 0, animations: {
                self.gameOver.frame.size = CGSize(width: 550, height: 375)
            })
        }

    }
    @IBOutlet weak var playAgainButton: UIButton!
    
    @IBAction func playAgain(_ sender: AnyObject)
    {
       gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        gameIsActive = true
        activePlayer = 1
        
        playAgainButton.isHidden = true
        label.isHidden = true
        
        gameOver.isHidden = true
        
        for i in 1...9
        {
            let button = view.viewWithTag(i) as! UIButton
            button.setImage(nil, for: UIControlState())
        }
        
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = SKColor.black
        
        //view.backgroundColor = SKColor.black
        
        if let view = self.view as! SKView? {
            // Load the SKScene from 'GameScene.sks'
            if let scene = SKScene(fileNamed: "GameScene") {
                // Set the scale mode to scale to fit the window
                scene.scaleMode = .aspectFill
                
                // Present the scene
                view.presentScene(scene)
            }
            
            view.ignoresSiblingOrder = true
           
          //  view.showsFPS = true
            //view.showsNodeCount = true
        }
    }

//    override var shouldAutorotate: Bool {
//        return true
//    }
//
//    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
//        if UIDevice.current.userInterfaceIdiom == .phone {
//            return .allButUpsideDown
//        } else {
//            return .all
//        }
//   }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
